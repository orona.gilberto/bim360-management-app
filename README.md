# bim360-management-app

## Description

- This is a POC front end application for resource management of the BIM360 Construction Managment Software.

## Setup

- Begin by cloning this repository to your IDE if you haven't already done so.
- Run 'npm install' to get the latest modules as annotated in the package.json
- Run 'npm start' to run the project, use a new branch for any new commit and merge when reviewed.
- Open [http://localhost:3000](http://localhost:3000) to view the running application, it should look like below.

<img height="300" src="./src/images/bimMain.PNG" width="700"/>
