import {GET_USERS} from "../../constants/actionConstants";
import axios from "axios";

export const getUsers = () => async dispatch => {
    try{
        const querystring = require('querystring');
        const callAuth = await axios.post("https://developer.api.autodesk.com/authentication/v1/authenticate", querystring.stringify({client_id:'oBfG5YVOfRVJvifNTdAYfGkF9t5YAaPx', client_secret:'BZoTfRe7ujAdGAdF', grant_type:'client_credentials', scope:'data:read'}),{
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            }
        })
        if (callAuth.data) {
            const token = callAuth.data.access_token;
            const call = await axios.get("https://developer.api.autodesk.com/project/v1/hubs/b.e6c0f9e8-d3fc-4558-a8ce-a4b710726ade/projects", {
                headers: {
                    Authorization: 'Bearer ' + token
                }
            })
            dispatch( {
                type: GET_USERS,
                payload: call.data
            })
        } else {
            console.log("Gilly you failed to get auth from the authentication service");
        }
    }
    catch (e) {
        console.log("Gilly there was an error with the get users service: " + e);
    }
}