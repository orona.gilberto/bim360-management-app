import {configureStore} from '@reduxjs/toolkit';
import usersReducer from "./reducers/users-reducer";

export default configureStore({
    reducer:{
        usersReducer: usersReducer
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            thunk: {},
            serializableCheck: false,
            ImmutableStateInvariantMiddleware: false
        }),
})