import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {getUsers} from "../redux/actions/users-action";
import "../css/pocMain.css";

class POCMain extends React.Component {
    componentDidMount() {
        this.props.getUsers();
    }

    render() {
        let projectsLoading = (
            <div className="loadingSpinner">
            </div>
        )

        // let valuesList = Object.values(this.props.users).map((value, index) => {
        //     return (
        //         <div key={index}>
        //             <div>{value}</div>
        //         </div>
        //     )
        //     }
        // )

        let projectsTable = (
            <div>
                <table>
                    <tr>
                        <th>Project Name</th>
                        <th>Project ID</th>
                    </tr>
                    {this.props.users?.map(project =>
                        <tr>
                            <td>{project.attributes.name}</td>
                            <td>{project.id}</td>
                        </tr>
                    )}
                </table>
            </div>
        )

        return (
            <div>
                <div>Gilly Place Service Calls in here for testing</div>
                {this.props.users ? projectsTable : projectsLoading}
            </div>
        )
    }
}

POCMain.propTypes = {
    getUsers: PropTypes.func
}

function mapStateToProps(state) {
    return {
       users: state.usersReducer?.users?.data,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getUsers: () => {
           dispatch(getUsers());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(POCMain);