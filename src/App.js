import './css/App.css';
import POCMain from "./components/POCMain";

function App() {
  return (
    <div className="App">
      <h1 >
          Test Project for BIM 360 User Management App
      </h1>
        <POCMain />
    </div>
  );
}

export default App;
